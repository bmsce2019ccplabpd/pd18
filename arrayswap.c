//Program to switch positions of smallest and largest elements of an array.

#include<stdio.h>

 int main()
{
  int a[15], i, s, l, n, lpos, spos, temp;
   printf("\nEnter the number of elements in the array: ");
    scanf("%d", &n);
   
   printf("\nEnter the array elements: ");
    for(i=0; i<n; ++i)
     scanf("%d ", &a[i]);
   printf("\n array elements: ");
    for(i=0; i<n; ++i)	
	printf("%d",a[i])
   
   s=a[0];
   l=a[0];
    for(i=1; i<n; ++i)
       if(a[i]>l)
        {
         l=a[i];
         lpos=i;
        }
   printf("\nThe largest is %d", l);
    for(i=1; i<n; ++i)
       if(a[i]<s)
        {
         s=a[i]; 
         spos=i;
        }  
   printf("\nThe smallest is %d", s);

     for(i=0;i<n;i++)
	if(a[i]>a[i+1])
	{	temp=a[i];
     		a[i]=a[i+1];
     		a[i+1]=temp;
	}

   printf("\nThe new array is : \n");
    for(i=0; i<n; i++)
     printf("%d\t", a[i]);
     printf("\n");

 return 0;
}